package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Sumario struct {
	TipoRegistro                              string 	`json:"TipoRegistro"`
	ValorTotalMercadorias                     float64	`json:"ValorTotalMercadorias"`
	ValorTotalIPI                             float64	`json:"ValorTotalIPI"`
	ValorTotalAbatimentos                     float64	`json:"ValorTotalAbatimentos"`
	ValorTotalEncargos                        float64	`json:"ValorTotalEncargos"`
	ValorTotalDescontosComerciais             float64	`json:"ValorTotalDescontosComerciais"`
	ValorTotalDespesasAcessoriasTributadas    float64	`json:"ValorTotalDespesasAcessoriasTributadas"`
	ValorTotalDespesasAcessoriasNaoTributadas float64	`json:"ValorTotalDespesasAcessoriasNaoTributadas"`
	ValorTotalPedido                          float64	`json:"ValorTotalPedido"`
}

func (s *Sumario) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesSumario

	err = posicaoParaValor.ReturnByType(&s.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalMercadorias, "ValorTotalMercadorias")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalIPI, "ValorTotalIPI")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalAbatimentos, "ValorTotalAbatimentos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalEncargos, "ValorTotalEncargos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalDescontosComerciais, "ValorTotalDescontosComerciais")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalDespesasAcessoriasTributadas, "ValorTotalDespesasAcessoriasTributadas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalDespesasAcessoriasNaoTributadas, "ValorTotalDespesasAcessoriasNaoTributadas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&s.ValorTotalPedido, "ValorTotalPedido")
	if err != nil {
		return err
	}


	return err
}

var PosicoesSumario = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 2, 0},
	"ValorTotalMercadorias":                      {2, 17, 2},
	"ValorTotalIPI":                      {17, 32, 2},
	"ValorTotalAbatimentos":                      {32, 47, 2},
	"ValorTotalEncargos":                      {47, 62, 2},
	"ValorTotalDescontosComerciais":                      {62, 77, 2},
	"ValorTotalDespesasAcessoriasTributadas":                      {77, 92, 2},
	"ValorTotalDespesasAcessoriasNaoTributadas":                      {92, 107, 2},
	"ValorTotalPedido":                      {107, 122, 2},
}