package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	TipoRegistro                  string `json:"TipoRegistro"`
	FuncaoMensagem                string `json:"FuncaoMensagem"`
	TipoPedido                    string `json:"TipoPedido"`
	NumeroPedidoComprador         string `json:"NumeroPedidoComprador"`
	NumeroPedidoSistemaEmissao    string `json:"NumeroPedidoSistemaEmissao"`
	DataHoraEmissaoPedido         int64  `json:"DataHoraEmissaoPedido"`
	DataHoraInicialPeriodoEntrega int64  `json:"DataHoraInicialPeriodoEntrega"`
	DataHoraFInalPeriodoEntrega   int64  `json:"DataHoraFInalPeriodoEntrega"`
	NumeroContrato                string `json:"NumeroContrato"`
	ListaPrecos                   string `json:"ListaPrecos"`
	EanLocalizacaoFornecedor      int64  `json:"EanLocalizacaoFornecedor"`
	EanLocalizacaoComprador       int64  `json:"EanLocalizacaoComprador"`
	EanLocalizacaoCobrancaFatura  int64  `json:"EanLocalizacaoCobrancaFatura"`
	EanLocalizacaoLocalEntrega    int64  `json:"EanLocalizacaoLocalEntrega"`
	CnpjFornecedor                int64  `json:"CnpjFornecedor"`
	CnpjComprador                 int64  `json:"CnpjComprador"`
	CnpjLocalCobrancaFatura       int64  `json:"CnpjLocalCobrancaFatura"`
	CnpjLocalEntrega              int64  `json:"CnpjLocalEntrega"`
	TipoCodigoTransportadora      string `json:"TipoCodigoTransportadora"`
	CodigoTransportadora          int64  `json:"CodigoTransportadora"`
	NomeTransportadora            string `json:"NomeTransportadora"`
	CondicaoEntrega               string `json:"CondicaoEntrega"`
	SecaoPedido                   int32  `json:"SecaoPedido"`
	ObeservacaoPedido             string `json:"ObeservacaoPedido"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.FuncaoMensagem, "FuncaoMensagem")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoPedido, "TipoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroPedidoComprador, "NumeroPedidoComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroPedidoSistemaEmissao, "NumeroPedidoSistemaEmissao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataHoraEmissaoPedido, "DataHoraEmissaoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataHoraInicialPeriodoEntrega, "DataHoraInicialPeriodoEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataHoraFInalPeriodoEntrega, "DataHoraFInalPeriodoEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroContrato, "NumeroContrato")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ListaPrecos, "ListaPrecos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EanLocalizacaoFornecedor, "EanLocalizacaoFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EanLocalizacaoComprador, "EanLocalizacaoComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EanLocalizacaoCobrancaFatura, "EanLocalizacaoCobrancaFatura")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EanLocalizacaoLocalEntrega, "EanLocalizacaoLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjFornecedor, "CnpjFornecedor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjComprador, "CnpjComprador")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjLocalCobrancaFatura, "CnpjLocalCobrancaFatura")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjLocalEntrega, "CnpjLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoCodigoTransportadora, "TipoCodigoTransportadora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoTransportadora, "CodigoTransportadora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NomeTransportadora, "NomeTransportadora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CondicaoEntrega, "CondicaoEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.SecaoPedido, "SecaoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ObeservacaoPedido, "ObeservacaoPedido")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                  {0, 2, 0},
	"FuncaoMensagem":                {2, 5, 0},
	"TipoPedido":                    {5, 8, 0},
	"NumeroPedidoComprador":         {8, 28, 0},
	"NumeroPedidoSistemaEmissao":    {28, 48, 0},
	"DataHoraEmissaoPedido":         {48, 60, 0},
	"DataHoraInicialPeriodoEntrega": {60, 72, 0},
	"DataHoraFInalPeriodoEntrega":   {72, 84, 0},
	"NumeroContrato":                {84, 99, 0},
	"ListaPrecos":                   {99, 114, 0},
	"EanLocalizacaoFornecedor":      {114, 127, 0},
	"EanLocalizacaoComprador":       {127, 140, 0},
	"EanLocalizacaoCobrancaFatura":  {140, 153, 0},
	"EanLocalizacaoLocalEntrega":    {153, 166, 0},
	"CnpjFornecedor":                {166, 180, 0},
	"CnpjComprador":                 {180, 194, 0},
	"CnpjLocalCobrancaFatura":       {194, 208, 0},
	"CnpjLocalEntrega":              {208, 222, 0},
	"TipoCodigoTransportadora":      {222, 225, 0},
	"CodigoTransportadora":          {225, 239, 0},
	"NomeTransportadora":            {239, 269, 0},
	"CondicaoEntrega":               {269, 272, 0},
	"SecaoPedido":                   {272, 275, 0},
	"ObeservacaoPedido":             {275, 315, 0},
}
