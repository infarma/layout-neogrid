package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Crossdocking struct {
	TipoRegistro                  string	`json:"TipoRegistro"`
	EanLocalEntrega               int64 	`json:"EanLocalEntrega"`
	CnpjLocalEntrega              int64 	`json:"CnpjLocalEntrega"`
	DataHoraInicialPeriodoEntrega int64 	`json:"DataHoraInicialPeriodoEntrega"`
	DataHoraFinalPeriodoEntrega   int64 	`json:"DataHoraFinalPeriodoEntrega"`
	Quantidade                    int64 	`json:"Quantidade"`
	UnidadeMedida                 string	`json:"UnidadeMedida"`
}

func (c *Crossdocking) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCrossdocking

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.EanLocalEntrega, "EanLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjLocalEntrega, "CnpjLocalEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataHoraInicialPeriodoEntrega, "DataHoraInicialPeriodoEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataHoraFinalPeriodoEntrega, "DataHoraFinalPeriodoEntrega")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.UnidadeMedida, "UnidadeMedida")
	if err != nil {
		return err
	}


	return err
}

var PosicoesCrossdocking = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 2, 0},
	"EanLocalEntrega":                      {2, 15, 0},
	"CnpjLocalEntrega":                      {15, 29, 0},
	"DataHoraInicialPeriodoEntrega":                      {29, 41, 0},
	"DataHoraFinalPeriodoEntrega":                      {41, 53, 0},
	"Quantidade":                      {53, 68, 0},
	"UnidadeMedida":                      {68, 71, 0},
}