package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Cabecalho               Cabecalho               `json:"Cabecalho"`
	Pagamento               Pagamento               `json:"Pagamento"`
	DescontosEncargosPedido DescontosEncargosPedido `json:"DescontosEncargosPedido"`
	Itens                   []Itens                 `json:"Itens"`
	Grade                   []Grade                 `json:"Grade"`
	Crossdocking            []Crossdocking          `json:"Crossdocking"`
	Sumario                 Sumario                 `json:"Sumario"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		var index int32
		if identificador == "01" {
			err = arquivo.Cabecalho.ComposeStruct(string(runes))
		} else if identificador == "02" {
			err = arquivo.Pagamento.ComposeStruct(string(runes))
		} else if identificador == "03" {
			err = arquivo.DescontosEncargosPedido.ComposeStruct(string(runes))
		} else if identificador == "04" {
			err = arquivo.Itens[index].ComposeStruct(string(runes))
			index++
		} else if identificador == "05" {
			err = arquivo.Grade[index].ComposeStruct(string(runes))
		} else if identificador == "06" {
			err = arquivo.Crossdocking[index].ComposeStruct(string(runes))
		} else if identificador == "09" {
			err = arquivo.Sumario.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
