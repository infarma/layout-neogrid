package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Pagamento struct {
	TipoRegistro                 string  `json:"TipoRegistro"`
	CondicaoPagamento            string  `json:"CondicaoPagamento"`
	ReferenciaData               string  `json:"ReferenciaData"`
	ReferenciaTempoData          string  `json:"ReferenciaTempoData"`
	TipoPeriodo                  string  `json:"TipoPeriodo"`
	NumeroPedidos                int32   `json:"NumeroPedidos"`
	DataVencimento               int32   `json:"DataVencimento"`
	ValorPagar                   float64 `json:"ValorPagar"`
	PercentualPagarValorFaturado float32 `json:"PercentualPagarValorFaturado"`
}

func (p *Pagamento) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesPagamento

	err = posicaoParaValor.ReturnByType(&p.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CondicaoPagamento, "CondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ReferenciaData, "ReferenciaData")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ReferenciaTempoData, "ReferenciaTempoData")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.TipoPeriodo, "TipoPeriodo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.NumeroPedidos, "NumeroPedidos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.DataVencimento, "DataVencimento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ValorPagar, "ValorPagar")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PercentualPagarValorFaturado, "PercentualPagarValorFaturado")
	if err != nil {
		return err
	}

	return err
}

var PosicoesPagamento = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                 {0, 2, 0},
	"CondicaoPagamento":            {2, 5, 0},
	"ReferenciaData":               {5, 8, 0},
	"ReferenciaTempoData":          {8, 11, 0},
	"TipoPeriodo":                  {11, 14, 0},
	"NumeroPedidos":                {14, 17, 0},
	"DataVencimento":               {17, 25, 0},
	"ValorPagar":                   {25, 40, 2},
	"PercentualPagarValorFaturado": {40, 45, 2},
}
