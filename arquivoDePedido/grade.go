package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Grade struct {
	TipoRegistro      string `json:"TipoRegistro"`
	TipoCodigoProduto string `json:"TipoCodigoProduto"`
	CodigoProduto     string `json:"CodigoProduto"`
	Quantidade        int64  `json:"Quantidade"`
	UnidadeMedida     string `json:"UnidadeMedida"`
}

func (g *Grade) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesGrade

	err = posicaoParaValor.ReturnByType(&g.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&g.TipoCodigoProduto, "TipoCodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&g.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&g.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&g.UnidadeMedida, "UnidadeMedida")
	if err != nil {
		return err
	}

	return err
}

var PosicoesGrade = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":      {0, 2, 0},
	"TipoCodigoProduto": {2, 5, 0},
	"CodigoProduto":     {5, 19, 0},
	"Quantidade":        {19, 34, 0},
	"UnidadeMedida":     {34, 37, 0},
}
