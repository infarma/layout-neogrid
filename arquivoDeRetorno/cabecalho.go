package arquivoDeRetorno

type Cabecalho struct {
	TipoRegistro                  string `json:"TipoRegistro"`
	FuncaoMensagem                string `json:"FuncaoMensagem"`
	TipoRespostaCodificado        string `json:"TipoRespostaCodificado"`
	NumeroRespostaPedidoCompra    string `json:"NumeroRespostaPedidoCompra"`
	DataHoraEmissao               int64  `json:"DataHoraEmissao"`
	DataHoraEntregaAtendida       int64  `json:"DataHoraEntregaAtendida"`
	NumeroPedidoCompra            string `json:"NumeroPedidoCompra"`
	NumeroAlteracaoPedidoCompra   string `json:"NumeroAlteracaoPedidoCompra"`
	EanLocalizacaoFornecedor      int64  `json:"EanLocalizacaoFornecedor"`
	EanLocalizacaoComprador       int64  `json:"EanLocalizacaoComprador"`
	EanLocalizacaoLocalEntrega    int64  `json:"EanLocalizacaoLocalEntrega"`
	CnpjFornecedor                int64  `json:"CnpjFornecedor"`
	CnpjComprador                 int64  `json:"CnpjComprador"`
	CnpjLocalEntrega              int64  `json:"CnpjLocalEntrega"`
	PrevisaoDataFaturamento       int64  `json:"PrevisaoDataFaturamento"`
	PrevisaoDataEntregaMercadoria int64  `json:"PrevisaoDataEntregaMercadoria"`
}
