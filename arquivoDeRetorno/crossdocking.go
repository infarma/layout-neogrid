package arquivoDeRetorno

type Crossdocking struct {
	TipoRegistro                  string `json:"TipoRegistro"`
	EanLocalEntrega               int64  `json:"EanLocalEntrega"`
	CnpjLocalEntrega              int64  `json:"CnpjLocalEntrega"`
	DataHoraInicialPeriodoEntrega int64  `json:"DataHoraInicialPeriodoEntrega"`
	DataHoraFinalPeriodoEntrega   int64  `json:"DataHoraFinalPeriodoEntrega"`
	Quantidade                    int64  `json:"Quantidade"`
	UnidadeMedida                 string `json:"UnidadeMedida"`
}
