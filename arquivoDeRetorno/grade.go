package arquivoDeRetorno

type Grade struct {
	TipoRegistro       string `json:"TipoRegistro"`
	TipoCodigoProduto  string `json:"TipoCodigoProduto"`
	CodigoProduto      string `json:"CodigoProduto"`
	QuantidadeAtentida int64  `json:"QuantidadeAtentida"`
	UnidadeMedida      string `json:"UnidadeMedida"`
}
