package arquivoDeRetorno

type Sumario struct {
	TipoRegistro                  string  `json:"TipoRegistro"`
	ValorTotalMercadorias         float64 `json:"ValorTotalMercadorias"`
	ValorTotalIPI                 float64 `json:"ValorTotalIPI"`
	ValorTotalAbatimentos         float64 `json:"ValorTotalAbatimentos"`
	ValorTotalEncargos            float64 `json:"ValorTotalEncargos"`
	ValorTotalDescontosComerciais float64 `json:"ValorTotalDescontosComerciais"`
	ValorTotalPedidoAtendido      float64 `json:"ValorTotalPedidoAtendido"`
}
