package arquivoDeNotaFiscal

type Sumario struct {
	TipoRegistro                               string  `json:"TipoRegistro"`
	NumeroLinhasNora                           int32   `json:"NumeroLinhasNora"`
	QuantidadeTotalEMbalagens                  int64   `json:"QuantidadeTotalEMbalagens"`
	PesoBrutoTotal                             float64 `json:"PesoBrutoTotal"`
	PesoLiquidoTotal                           float64 `json:"PesoLiquidoTotal"`
	CubagemTotal                               float64 `json:"CubagemTotal"`
	ValorTotalLinhasNota                       float64 `json:"ValorTotalLinhasNota"`
	ValorTotalDescontos                        float64 `json:"ValorTotalDescontos"`
	ValorTotalENcargos                         float64 `json:"ValorTotalENcargos"`
	ValorTotalAbatimentos                      float64 `json:"ValorTotalAbatimentos"`
	ValorTotalFrete                            float64 `json:"ValorTotalFrete"`
	ValorTotalSeguro                           float64 `json:"ValorTotalSeguro"`
	ValorDespesasAcessorias                    float64 `json:"ValorDespesasAcessorias"`
	ValorBaseCalculoICMS                       float64 `json:"ValorBaseCalculoICMS"`
	ValorTotalICMS                             float64 `json:"ValorTotalICMS"`
	ValorBaseCalculoICMSSubstituicaoTributaria float64 `json:"ValorBaseCalculoICMSSubstituicaoTributaria"`
	ValorTotalICMSSubstituicaoTributaria       float64 `json:"ValorTotalICMSSubstituicaoTributaria"`
	ValorBaseCalculoICMSReducaoTributaria      float64 `json:"ValorBaseCalculoICMSReducaoTributaria"`
	ValorTotalICMSReducaoTributaria            float64 `json:"ValorTotalICMSReducaoTributaria"`
	ValorBaseCalculoIPI                        float64 `json:"ValorBaseCalculoIPI"`
	ValorTotalIPI                              float64 `json:"ValorTotalIPI"`
	ValorTotalNota                             float64 `json:"ValorTotalNota"`
}
