package arquivoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Pagamento struct {
	TipoRegistro                    string 	`json:"TipoRegistro"`
	CondicaoPagamento               string 	`json:"CondicaoPagamento"`
	RefenrenciaData                 string 	`json:"RefenrenciaData"`
	ReferenciaTempo                 string 	`json:"ReferenciaTempo"`
	TipoPeriodo                     string 	`json:"TipoPeriodo"`
	NumeroPedidos                   int32  	`json:"NumeroPedidos"`
	DataVencimento                  int64  	`json:"DataVencimento"`
	TipoPercentualCondicaoPagamento string 	`json:"TipoPercentualCondicaoPagamento"`
	PercentualCondicaoPagamento     float32	`json:"PercentualCondicaoPagamento"`
	TipoValorCondicaoPagamento      string 	`json:"TipoValorCondicaoPagamento"`
	ValorCondicaoPagamento          float64	`json:"ValorCondicaoPagamento"`
}

func (p *Pagamento) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesPagamento

	err = posicaoParaValor.ReturnByType(&p.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CondicaoPagamento, "CondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.RefenrenciaData, "RefenrenciaData")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ReferenciaTempo, "ReferenciaTempo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.TipoPeriodo, "TipoPeriodo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.NumeroPedidos, "NumeroPedidos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.DataVencimento, "DataVencimento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.TipoPercentualCondicaoPagamento, "TipoPercentualCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.PercentualCondicaoPagamento, "PercentualCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.TipoValorCondicaoPagamento, "TipoValorCondicaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.ValorCondicaoPagamento, "ValorCondicaoPagamento")
	if err != nil {
		return err
	}


	return err
}

var PosicoesPagamento = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":                      {0, 2, 0},
	"CondicaoPagamento":                      {2, 5, 0},
	"RefenrenciaData":                      {5, 8, 0},
	"ReferenciaTempo":                      {8, 11, 0},
	"TipoPeriodo":                      {11, 14, 0},
	"NumeroPedidos":                      {14, 17, 0},
	"DataVencimento":                      {17, 25, 0},
	"TipoPercentualCondicaoPagamento":                      {25, 28, 0},
	"PercentualCondicaoPagamento":                      {28, 33, 2},
	"TipoValorCondicaoPagamento":                      {33, 36, 0},
	"ValorCondicaoPagamento":                      {36, 51, 2},
}