package arquivoDeNotaFiscal

type Cabecalho struct {
	TipoRegistro                    string `json:"TipoRegistro"`
	FuncaoMensgem                   string `json:"FuncaoMensgem"`
	TipoNota                        string `json:"TipoNota"`
	NumeroNotaFiscal                int64  `json:"NumeroNotaFiscal"`
	SerieNotaFiscal                 int32  `json:"SerieNotaFiscal"`
	SubserieNotaFiscal              string `json:"SubserieNotaFiscal"`
	DataHoraEmissaoNotaFiscal       int64  `json:"DataHoraEmissaoNotaFiscal"`
	DataHoraDespachoSaida           int64  `json:"DataHoraDespachoSaida"`
	DataHoraEntrega                 int64  `json:"DataHoraEntrega"`
	CodigoFiscalOperacoesPrestacoes string `json:"CodigoFiscalOperacoesPrestacoes"`
	NumeroPedidoComprador           string `json:"NumeroPedidoComprador"`
	NumeroPedidoSistemaEmissao      string `json:"NumeroPedidoSistemaEmissao"`
	NumeroContato                   string `json:"NumeroContato"`
	ListaPrecos                     string `json:"ListaPrecos"`
	EanLocalizacaoComprador         int64  `json:"EanLocalizacaoComprador"`
	EanLocalizacaoCobrancaFatura    int64  `json:"EanLocalizacaoCobrancaFatura"`
	EanLocalizacaoLocalEntrega      int64  `json:"EanLocalizacaoLocalEntrega"`
	EanLocalizacaoFornecedor        int64  `json:"EanLocalizacaoFornecedor"`
	EanLocalizacaoEmissorNota       int64  `json:"EanLocalizacaoEmissorNota"`
	CnpjComprador                   int64  `json:"CnpjComprador"`
	CnpjLocalCobrancaFatura         int64  `json:"CnpjLocalCobrancaFatura"`
	CnpjLocalEntrega                int64  `json:"CnpjLocalEntrega"`
	CnpjFornecedor                  int64  `json:"CnpjFornecedor"`
	CnpjEmissorNota                 int64  `json:"CnpjEmissorNota"`
	EstadoEmissorNota               string `json:"EstadoEmissorNota"`
	InscricaoEstadualEmissorNota    string `json:"InscricaoEstadualEmissorNota"`
	TipoCodigoTransportadora        string `json:"TipoCodigoTransportadora"`
	CodigoTrnasportadora            int64  `json:"CodigoTrnasportadora"`
	NomeTransportadora              string `json:"NomeTransportadora"`
	CondicaoEntrega                 string `json:"CondicaoEntrega"`
	ChaveAcessoNotaFiscal           string `json:"ChaveAcessoNotaFiscal"`
}
