package arquivoDeNotaFiscal

type DescontosEncargosNota struct {
	TipoRegistro                  string  `json:"TipoRegistro"`
	PercentualDescontoFinanceiro  float32 `json:"PercentualDescontoFinanceiro"`
	ValorDescontoFinanceiro       float64 `json:"ValorDescontoFinanceiro"`
	PercentualDescontoComercial   float32 `json:"PercentualDescontoComercial"`
	ValorDescontoComercial        float64 `json:"ValorDescontoComercial"`
	PercentualDescontoPromocional float32 `json:"PercentualDescontoPromocional"`
	ValorDescontoPromocional      float64 `json:"ValorDescontoPromocional"`
	PercentualEncargosFinanceiros float32 `json:"PercentualEncargosFinanceiros"`
	ValorEncargosFinanceiros      float64 `json:"ValorEncargosFinanceiros"`
	PercentualEncargosFrete       float32 `json:"PercentualEncargosFrete"`
	ValorEncargosFrete            float64 `json:"ValorEncargosFrete"`
	PercentualEncargosSeguro      float32 `json:"PercentualEncargosSeguro"`
	ValorEncargosSeguro           float64 `json:"ValorEncargosSeguro"`
}
